#!/bin/bash

sed -i "s/.emscripten { padding-right: 0; margin-left: auto; margin-right: auto; display: block/.emscripten { padding-right: 0; margin-left: auto; margin-right: auto; display: block; position: absolute; height: 95% /" index.html
sed -i "s/body {/a { display: none } span { display: none } textarea { display: none } body {/" index.html
sed -i "98s/display: block//" index.html

