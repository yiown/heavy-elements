Heavy Elements
==============

Vagrant based build:
------------------
You will need VirtualBox installed.

$ vagrant up --provider virtualbox

Around 2hs later ...

$ ln -s ../src src
$ vagrant ssh -c "cd urho3d-project;./cmake_emscripten .;make;/vagrant/cleanBuild.sh"

You can now access the build through your web browser on http://<ip address>/urho
To get the ip address:

$ vagrant ssh -c "ifconfig"
