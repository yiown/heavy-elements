Heavy Elements
==============

3-in-line 1-minute speed game, with a chemistry theme, and a heavy metal twist.

Gameplay:
---------
Select or drag elements to apply quantum waves and switch the element places inside the reactor chamber.
Due to the position of the magnets, only horizontal or vertical switches are supported.
A switch resulting in 3 or more reactible elements in line, will trigger a chain reaction of the elements.
Conversely if no reaction is triggered, the opposing forces will switch back the elements.

Elements in a reaction will turn into energy. You can see the amount of energy produced in the reactor display.
As elements react, more elements will be injected from the top of the reactor to keep the chamber full.
Each element type have unique reactive capabilities. As a rule of thumb, every element reacts to its own type.

A dark matter active isotope is powering the Polywell chamber.
Once the isotope decays, reactions will stop.
The dark matter will last for about 60 seconds. Use them well.

Reactions energy output:
------------------------
- 3 elements: 25 kw
- 4 elements: 50 kw
- 5 elements: 75 kw
- 6 elements: 150 kw
- 7 elements: 300 kw

Requirements:
-------------
- 8x8 grid.
- 1 minute long.
- 5 elements.
- Drag and click must work.
- 3 or more in line disappear.
- Move with no coincidences rolls back.
- Collapses from the top.
- Moves can be up, down, left, right.

Goals:
------
- STL based C++11 code.
- Technology agnostic game core code.
- Cross platform support.

Architecture:
-------------
The code is based on 2 well defined layers:

- HE* : These classes involve the game code itself. Including supporting utilities like Vec2D. Only STL allowed at this level.
- U* : These classes comprehend the underlying engine system. It deals with hardware specifics, C objects and related lower level code.

Many concepts have two part definitions, one in game terms and the other in engine terms.
For instance, the element have its game logic in the HE layer and its particular implementation in the U layer, connected through inheritance.

There is a hierarchy for element types and 5 elements already implemented.
Polymorphism is used for element reaction capabilities.

Technologies:
-------------
The game is technology agnostic and can run in any game engine simply by replacing the thin U layer.
Preprocessor definitions can be used to switch between technologies.

The current U layer is based on the Urho3D engine, since it provides the following features:
- Straightforward abstractions based on a node tree model.
- Simple and easy modern C++ code.
- Actually working multiplatform support including web based builds.

Acknowledgements:
-----------------
Every piece of technology used in the game is under GPL or similar licences, assets included.

Troubleshoot:
-------------
- Black window: Video drivers mismatch, try using the backward compatibility with OpenGL 2.1 adding the command line parameter "-gl2".

Backlog:
--------
- Reactions build up.
  * 100 elements: +500 kw once
  * 200 elements: +750 kw once
  * 300 elements: +1000 kw once
  * 400 elements: +1250 kw once
  * 500 elements: +1500 kw once
- Combo multipliers.
  * combo 2: +50 kw each reaction
  * combo 3: +100 kw each reaction
  * combo 4: +150 kw each reaction
  * combo 5: +250 kw each reaction
  * combo 6: +350 kw each reaction
  * combo 7: +500 kw each reaction
  * combo 8: +750 kw each reaction
  * combo 9+: +1000 kw each reaction
- Powerups based on heavier elements.
- Disable sound button.
- Disable music button.
- Reactor shutdown button.
- Pause button before new session.
- Sound assets for reactions of 5,6,7

