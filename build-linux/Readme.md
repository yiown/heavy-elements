Heavy Elements
==============

CMake based build:
------------------
$ export URHO3D_HOME=<your urho3d location>
$ cmake .
$ make -j<your cpu count>

IDE support:
------------
Just open the CMakeLists.txt file.