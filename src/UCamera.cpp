#include <UCamera.h>
#include <UApp.h>

/** Constructor creating the camera node. */
UCamera::UCamera() {
    // setup camera
    cameraNode_ = app->createSceneChild("Camera");
    auto camera_ = cameraNode_->CreateComponent< Urho3D::Camera >();
    camera_->SetFarClip(2000);
    app->setActiveCamera(camera_);
}

/** Set the zoom as a camera movement. */
void UCamera::setZoom(float zoom) const {
    cameraNode_->SetPosition(Urho3D::Vector3(2, 5, zoom));
}

/** Get the zoom as a camera movement. */
float UCamera::getZoom() const {
    return cameraNode_->GetPosition().z_;
}

