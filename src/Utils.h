#ifndef UTILS_H
#define UTILS_H
#include <sstream>

/**
 * @brief Convert a value to string.
 * @note Alternative to std::to_string for it does not yet come with every compiler.
 * @param value Value to convert.
 * @return String with the value.
 */
template < typename T >
std::string toString(T value, unsigned precision) {
    std::ostringstream os;
    os.precision(precision);
    os << std::fixed << value;
    return os.str();
}

#endif // UTILS_H


