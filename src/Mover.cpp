#include <Mover.h>

/** Default constructor initializing values. */
Mover::Mover() : destPos(0, 0), distance(0, 0), millisOut(0) {
    timeout = std::chrono::high_resolution_clock::now();
}

/** Simple destructor. */
Mover::~Mover() {}

/** Setup and start the movement. */
void Mover::set(const Vec2D& pos, const Vec2D& dest, unsigned millis) {
    destPos = dest;
    millisOut = millis;
    distance = dest - pos;
    timeout = std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(millisOut);
}

/** Calculate the current position in time. */
Vec2D Mover::getPos() const {
    if (done())
        return destPos;
    else {
        auto remaining = timeout - std::chrono::high_resolution_clock::now();
        auto millisLeft = std::chrono::duration_cast< std::chrono::milliseconds >(remaining).count();
        return calculate(unsigned(millisLeft));
    }
}

/** Check timeout reached. */
bool Mover::done() const {
    return std::chrono::high_resolution_clock::now() > timeout;
}

