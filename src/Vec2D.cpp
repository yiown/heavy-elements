#include <Vec2D.h>
#include <cmath>

/** Constructor initializing default values. */
Vec2D::Vec2D() : x(0), y(0) {}

/** Constructor initializing from given values. */
Vec2D::Vec2D(float x, float y) : x(x), y(y) {}

/** Add by coordinates. */
Vec2D Vec2D::operator+(const Vec2D& v) const {
    return Vec2D(x + v.x, y + v.y);
}

/** Substract by coordinates. */
Vec2D Vec2D::operator-(const Vec2D& v) const {
    return Vec2D(x - v.x, y - v.y);
}

/** Divide by constant. */
Vec2D Vec2D::operator/(float k) const {
    return Vec2D(x / k, y / k);
}

/** Multiply by constant. */
Vec2D Vec2D::operator*(float k) const {
    return Vec2D(x * k, y * k);
}

/** Less than or equal by coordinates. */
bool Vec2D::operator<(const Vec2D& v) const {
    return x < v.x && y < v.y;
}

/** Less than by coordinates. */
bool Vec2D::operator<=(const Vec2D& v) const {
    return x <= v.x && y <= v.y;
}

/** Pythagoras over distance. */
float Vec2D::distance(const Vec2D& v) const {
    return std::sqrt(std::pow(v.x - x, 2) + std::pow(v.y - y, 2));
}

/** Add one unit in the direction specified. */
Vec2D Vec2D::add(const Direction d) const {
    Vec2D result;
    switch (d) {
    case LEFT:
        result = Vec2D(x - 1, y);
        break;
    case RIGHT:
        result = Vec2D(x + 1, y);
        break;
    case UP:
        result = Vec2D(x, y + 1);
        break;
    case DOWN:
        result = Vec2D(x, y - 1);
        break;
    }
    return result;
}

