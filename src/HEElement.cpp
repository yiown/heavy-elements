#include <HEElement.h>
#include <HEState.h>
#include <algorithm>

/** Constructor passing values up. */
HEElement::HEElement(const std::string& name, const Vec2D& pos) : UElement(name, pos) {}

/** Activate state only if not already active. */
void HEElement::addState(HEState state) {
    if (!hasState(state)) {
        states.push_back(state);
        UElement::addState(state);
    }
}

/** Remove state only if active. */
void HEElement::removeState(HEState state) {
    auto s = std::find(states.begin(), states.end(), state);
    if (s != states.end()) {
        states.erase(s);
        UElement::removeState(state);
    }
}

/** Check for state. */
bool HEElement::hasState(HEState state) {
    return std::find(states.begin(), states.end(), state) != states.end();
}

/** Check not dropping or dropped. */
bool HEElement::isAlive() {
    return std::find_if(states.begin(), states.end(), [](HEState s) {
                            return s == HEState::DROPING || s == HEState::DROPPED;
                        }) == states.end();
}

/** Update status and position. */
void HEElement::update() {
    if (hasState(HEState::MOVING)) {
        setPos(slideMover.getPos());
        if (slideMover.done())
            removeState(HEState::MOVING);
    } else if (hasState(HEState::DROPING)) {
        setScale(dropMover.getPos().x);
        if (dropMover.done()) {
            removeState(HEState::DROPING);
            addState(HEState::DROPPED);
        }
    }
}

/** Drop this element setting up animation. */
void HEElement::drop(const unsigned millis) {
    dropMover.set(Vec2D(1.0f, 0.0f), Vec2D(0.0f, 0.0f), millis);
    addState(HEState::DROPING);
}

/** Slide this element setting up animation. */
void HEElement::slideTo(const Vec2D& newPos, const unsigned millis) {
    slideMover.set(getPos(), newPos, millis);
    addState(HEState::MOVING);
}

/** React to itself by default. */
bool HEElement::reactsTo(const std::shared_ptr< HEElement >& element) const {
    return name == element->name;
}

