#ifndef HECAMERA_H
#define HECAMERA_H
#include <UCamera.h>
#include <MoverSlideIn.h>

/** Camera handler. */
class HECamera : public UCamera {
private:
    /** @brief Camera mover. */
    MoverSlideIn mover;

public:
    /** @brief Camera constructor. */
    HECamera();

    /** @brief Update camera values. */
    void update() const;

    /**
     * @brief Zoom camera with animation.
     * @param zoom Final zoom value.
     * @param millis Animation time.
     */
    void slideZoom(float zoom, unsigned millis);
};

#endif // HECAMERA_H


