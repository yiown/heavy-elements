#include <HECamera.h>

/** Constructor initializing the underlaying camera. */
HECamera::HECamera() : UCamera() {}

/** Update camera values. */
void HECamera::update() const {
    setZoom(mover.getPos().x);
}

/** Animate camera zoom. */
void HECamera::slideZoom(float zoom, unsigned millis) {
    mover.set(Vec2D(getZoom(), 0.0f), Vec2D(zoom, 0.0f), millis);
}

