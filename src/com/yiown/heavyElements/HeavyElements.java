package com.yiown.heavyElements;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.libsdl.app.SDLActivity;

import android.content.Intent;

public class HeavyElements extends SDLActivity {

    @Override
    protected boolean onLoadLibrary(ArrayList<String> libraryNames) {
        libraryNames.add("heavy-elements");
        return super.onLoadLibrary(libraryNames);
    }

}
