#ifndef HENITROGEN_H
#define HENITROGEN_H
#include <HEElement.h>

/** @brief Nitrogen element. */
class HENitrogen : public HEElement {
public:
    /**
     * @brief Constructor initializing values.
     * @param pos Initial position.
     */
    explicit HENitrogen(const Vec2D& pos) : HEElement("N", pos) { }
};

#endif // HENITROGEN_H


