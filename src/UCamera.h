#ifndef UCAMERA_H
#define UCAMERA_H
#include <Urho3D/Graphics/Camera.h>

/** Urho3D camera component handler. */
class UCamera {
private:
    /** @brief Camera node. */
    Urho3D::Node* cameraNode_;

public:
    /** @brief Constructor. */
    UCamera();

    /**
     * @brief Zoom setter.
     * @param zoom Zoom to set.
     */
    void setZoom(float zoom) const;

    /**
     * @brief Zoom getter.
     * @return Zoom value.
     */
    float getZoom() const;
};

#endif // UCAMERA_H


