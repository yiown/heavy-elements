#ifndef UGAUGE_H
#define UGAUGE_H
#include <Urho3D/Scene/Node.h>
#include <Urho3D/Urho2D/ParticleEmitter2D.h>
#include <Urho3D/UI/Text3D.h>
#include <Vec2D.h>
#include <string>

/** Urho3D gauge. */
class UGauge {
private:
    /** @brief Urho3D node for the gauge. */
    Urho3D::Node* node_;
    /** @brief Urho3D effect node. */
    Urho3D::ParticleEmitter2D* emitter_;
    /** @brief Urho3D text node to display gauge value. */
    Urho3D::Text3D* text_;

public:
    /**
     * @brief Gauge constructor initializing values.
     * @param pos Initial position.
     */
    explicit UGauge(const Vec2D& pos);

    /** @brief Gauge destructor. */
    virtual ~UGauge();

    /**
     * @brief Value setter.
     * @param value Value to set.
     * @param text Text to display.
     */
    void setValue(unsigned value, std::string text) const;
};

#endif // UGAUGE_H


