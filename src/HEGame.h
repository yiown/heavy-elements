#ifndef HEGAME_H
#define HEGAME_H
#include <HEBoard.h>
#include <HECamera.h>
#include <HEChamber.h>

/** Game main entry point. */
class HEGame {
private:
    /** Game possible stages. */
    enum GameStage {
        INIT,
        SPLASH,
        TUTORIAL,
        CHAMBER,
        STATS
    };

    /** @brief Current stage. */
    GameStage stage;
    /** @brief Stage timeout. */
    std::chrono::time_point< std::chrono::high_resolution_clock > timeout;

    /** @brief Camera object. */
    std::unique_ptr< HECamera > camera;
    /** @brief Reaction chamber object. */
    std::unique_ptr< HEChamber > chamber;
    /** @brief Background music object. */
    std::shared_ptr< HESound > music;
    /** @brief Text container. */
    std::vector< std::unique_ptr< HEText > > texts;

    /** @brief Board object. */
    std::shared_ptr< HEBoard > board;

    /** @brief Advance to next game stage. */
    void nextStage();

public:
    /** @brief Game constructor. */
    HEGame();

    /** @brief Start the board play. */
    void start();

    /** @brief Update the board elements. */
    void update();

    /**
     * @brief Select by ID.
     * @param heId ID of the selection.
     */
    void select(unsigned heId);

    /**
    * @brief Drag the selection.
    * @param d Direction to drag to.
    */
    void selDrag(Direction d) const;
};

#endif // HEGAME_H


