#ifndef HEELEMENT_H
#define HEELEMENT_H
#include <memory>
#include <vector>
#include <UElement.h>
#include <MoverLineal.h>
#include <MoverSlideIn.h>

/** Element base class. */
class HEElement : public UElement {
private:
    /** @brief Element active states. */
    std::vector< HEState > states;

    /** @brief Slide animation. */
    MoverSlideIn slideMover;
    /** @brief Drop animation. */
    MoverLineal dropMover;

public:
    /**
     * @brief Constructor initializing values.
     * @param name Element name.
     * @param pos Element initial position.
     */
    HEElement(const std::string& name, const Vec2D& pos);

    /**
     * @brief Add active state.
     * @param state State to activate.
     */
    void addState(HEState state) override;
    /**
     * @brief Remove active state.
     * @param state State to remove.
     */
    void removeState(HEState state) override;
    /**
     * @brief Check active state.
     * @param state State to check.
     * @return True if active.
     */
    bool hasState(HEState state);
    /**
     * @brief Check element alive status.
     * @return True if alive.
     */
    bool isAlive();

    /** @brief Update the element internal processes. */
    virtual void update();

    /**
     * @brief Drop this element.
     * @param millis Millis for drop animation.
     */
    void drop(const unsigned millis);
    /**
     * @brief Slide into position.
     * @param newPos Destination.
     * @param millis Millis for slide animation.
     */
    void slideTo(const Vec2D& newPos, const unsigned millis);

    /**
     * @brief Check elements reactions.
     * @param element Element to check against.
     * @return True under reaction.
     */
    virtual bool reactsTo(const std::shared_ptr< HEElement >& element) const;
};

#endif // HEELEMENT_H


