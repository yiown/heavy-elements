#include <MoverSlideIn.h>
#include <math.h>

/** Inverse cos based movement implementation. */
Vec2D MoverSlideIn::calculate(unsigned millisLeft) const {
    return destPos - distance * (1.0f - cos(float(millisLeft) * float(M_PI_2) / float(millisOut)));
}

