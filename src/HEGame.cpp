#include <HEGame.h>
#include <Utils.h>

/** Handle stage transitions. */
void HEGame::nextStage() {
    // clean texts
    texts.clear();
    // advance stage
    switch (stage) {
    case GameStage::INIT:
        stage = GameStage::SPLASH;
        timeout = std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(3300);
        music = std::shared_ptr< HESound >(new HESound("background", true));
        camera->slideZoom(-45, 500);
        texts.emplace_back(new HEText(Vec2D(0, 0), "   \nHEAVY\nELEMENTS\n        ", 600, 3000, true));
        break;
    case GameStage::SPLASH:
        stage = GameStage::TUTORIAL;
        timeout = std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(15000);
        camera->slideZoom(-30, 500);
        texts.emplace_back(new HEText(Vec2D(1, 0),
                                      "                                                                 \n"
                                      "Elements in a chain of 3 or more will react and turn into energy.\n"
                                      "As elements react, more will be injected into the chamber.       \n"
                                      "As a rule of thumb, every element reacts to its own type.        \n"
                                      "                                                                 \n"
                                      "A dark matter isotope powers the chamber for 60 seconds.         \n"
                                      "Once the isotope decays, reactions will stop.                    \n"
                                      "                                                                 \n"
                                      "STARTING REACTOR ...          \n"
                                      "3 ...                         \n"
                                      "2 ...                         \n"
                                      "1 ...                         \n"
                                      "GO !!!                        \n",
                                      90, 15000, true));
        break;
    case GameStage::TUTORIAL:
        stage = GameStage::CHAMBER;
        timeout = std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(63000);
        camera->slideZoom(-20, 500);
        board = std::shared_ptr< HEBoard >(new HEBoard(8, 8));
        board->start();
        break;
    case GameStage::CHAMBER:
        stage = GameStage::STATS;
        timeout = std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(10000);
        music.reset();
        camera->slideZoom(-30, 500);
        texts.emplace_back(new HEText(Vec2D(0, -3), toString(board->getScore(), 0) +
                                      " KW added to your quota.\n"
                                      "REACTOR COOLING DOWN         \n"
                                      "New session starting ...     \n",
                                      90, 5000, true));
        break;
    case GameStage::STATS:
        stage = GameStage::INIT;
        timeout = std::chrono::high_resolution_clock::now();
        board.reset();
        break;
    }
}

/** Constructor initialing internal values. */
HEGame::HEGame() : stage(GameStage::INIT) {}

/** Start game. */
void HEGame::start() {
    // setup camera
    camera = std::unique_ptr< HECamera >(new HECamera());
    // setup chamber  
    chamber = std::unique_ptr< HEChamber >(new HEChamber());
    // advance stage
    nextStage();
}

/** Update game objects. */
void HEGame::update() {
    // update camera
    camera->update();
    // update texts
    for (auto& t : texts)
        t->update();
    // update board
    if (stage == GameStage::CHAMBER)
        board->update();
    // check stage timeout  
    if (timeout < std::chrono::high_resolution_clock::now())
        nextStage();
}

/** Process object selection. */
void HEGame::select(unsigned heId) {
    if (stage == GameStage::CHAMBER)
        board->select(heId);
    else
        timeout = std::chrono::high_resolution_clock::now();
}

/** Process selection dragging. */
void HEGame::selDrag(Direction d) const {
    if (stage == GameStage::CHAMBER)
        board->selDrag(d);
}

