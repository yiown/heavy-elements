#ifndef USOUND_H
#define USOUND_H
#include <string>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/Audio/SoundSource.h>

/** Underlaying sound abstraction. */
class USound {
private:
    /** @brief Urho3D node for the sound source. */
    Urho3D::Node* node_;
    /** @brief Urho3D sound source. */
    Urho3D::SoundSource* soundSource_;

public:
    /**
     * @brief Constructor initializing values.
     * @param name Sound name.
     * @param looped Play looped.
     */
    USound(std::string name, bool looped);

    /** @brief Destructor. */
    virtual ~USound();

    /**
     * @brief Get playing state.
     * @return True if playing.
     */
    virtual bool isPlaying();
};

#endif // USOUND_H


