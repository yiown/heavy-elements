#include <UText.h>
#include <UApp.h>
#include <Urho3D/UI/Font.h>

/** Constructor initializing the Urho3D text node. */
UText::UText(const Vec2D& pos, unsigned fontSize) {
    // node
    node_ = app->createSceneChild("Text");
    node_->SetPosition(Urho3D::Vector3(pos.x, pos.y, 0));
    if (fontSize > 100)
        node_->SetScale(fontSize / 100.0f);
    // text
    text_ = node_->CreateComponent< Urho3D::Text3D >();
    text_->SetFont(app->getResource< Urho3D::Font >("Fonts/digital-7 mono italic.ttf"), fontSize);
    text_->SetAlignment(Urho3D::HA_CENTER, Urho3D::VA_CENTER);
    text_->SetColor(Urho3D::Color::CYAN);
}

/** Destructor releasing the Urho3D node. */
UText::~UText() {
    node_->Remove();
}

/** Text setter in the text node. */
void UText::setText(std::string text) const {
    text_->SetText(text.c_str());
}

/** Text color setter in the text node. */
void UText::setColor(float r, float g, float b) const {
    text_->SetColor(Urho3D::Color(r, g, b));
}

