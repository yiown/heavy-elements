#include <UElement.h>
#include <UApp.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Urho2D/ParticleEmitter2D.h>
#include <Urho3D/Urho2D/ParticleEffect2D.h>

/** Add effect from resource to node. */
void UElement::addEffectToNode(HEState state, std::string res) const {
    auto childNode_ = node_->CreateChild(toString(state).c_str());
    auto emitter_ = childNode_->CreateComponent< Urho3D::ParticleEmitter2D >();
    emitter_->SetEffect(app->getResource< Urho3D::ParticleEffect2D >(res));
    emitter_->SetEnabled(false);
}

/** Constructor creating the Urho3D node. */
UElement::UElement(const std::string& name, const Vec2D& pos) : name(name) {
    // node
    node_ = app->createSceneChild("Element" + name);
    setPos(pos);
    // shape
    auto model_ = node_->CreateComponent< Urho3D::StaticModel >();
    model_->SetModel(app->getResource< Urho3D::Model >("Models/Sphere.mdl"));
    model_->SetMaterial(app->getResource< Urho3D::Material >("Materials/" + name + ".xml"));
    // effects
    addEffectToNode(HEState::SELECTED, "Effects/selected.pex");
    addEffectToNode(HEState::REACT, "Effects/react.pex");
}

/** Destructor releasing the Urho3D node. */
UElement::~UElement() {
    node_->Remove();
}

/** Set the ID directly into an Urho3D custom variable. */
void UElement::setId(unsigned id) const {
    node_->SetVar(KEY_ID.c_str(), id);
}

/** Get the ID from the Urho3D custom variable. */
unsigned UElement::getId() const {
    return node_->GetVar(KEY_ID.c_str()).GetUInt();
}

/** Get the position from the Urho3D node. */
Vec2D UElement::getPos() const {
    auto p = node_->GetPosition();
    return Vec2D(p.x_, p.y_);
}

/** Set the Urho3D node position. */
void UElement::setPos(const Vec2D& pos) const {
    node_->SetPosition(Urho3D::Vector3(pos.x, pos.y, 0));
}

/** Scale Urho3D node. */
void UElement::setScale(float scale) const {
    node_->SetScale(scale);
}

/** Enable node component according to state. */
void UElement::addState(HEState state) {
    auto child_ = node_->GetChild(toString(state).c_str());
    if (child_)
        child_->GetComponents()[0]->SetEnabled(true);
}

/** Disable node component according to state. */
void UElement::removeState(HEState state) {
    auto child_ = node_->GetChild(toString(state).c_str());
    if (child_)
        child_->GetComponents()[0]->SetEnabled(false);
}

