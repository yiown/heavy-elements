#include <UChamber.h>
#include <UApp.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Zone.h>

/** Create the node and model for the reaction chamber. */
UChamber::UChamber() {
    // setup the chamber model
    chamberNode_ = app->createSceneChild("Chamber");
    auto chamberModel_ = chamberNode_->CreateComponent< Urho3D::StaticModel >();
    chamberModel_->SetModel(app->getResource< Urho3D::Model >("Models/Box.mdl"));
    chamberModel_->SetMaterial(app->getResource< Urho3D::Material >("Materials/Chamber.xml"));
    chamberNode_->SetPosition(Urho3D::Vector3(8 / 2.0f * 1.5f, 7 / 2.0f * 1.5f, 34));
    chamberNode_->SetScale(67);
    chamberNode_->SetVar(KEY_ID.c_str(), 65536);

    // setup the fog ambient  
    zoneNode_ = app->createSceneChild("Zone");
    auto zone_ = zoneNode_->CreateComponent< Urho3D::Zone >();
    zone_->SetBoundingBox(Urho3D::BoundingBox(-40.0f, 40.0f));
    zone_->SetFogColor(Urho3D::Color(0.25f, 0.0f, 0.5f));
    zone_->SetFogStart(0.0f);
    zone_->SetFogEnd(100.0f);
}

