#ifndef HETEXT_H
#define HETEXT_H
#include <chrono>
#include <UText.h>

/** UI text string. */
class HEText : public UText {
private:
    /** @brief Final time point. */
    std::chrono::time_point< std::chrono::high_resolution_clock > ttl;
    /** @brief Animated text. */
    bool animate;
    /** @brief Animation ratio. */
    float animRatio;
    /** @brief Full text message. */
    std::string text;

public:
    /**
     * @brief Constructor initializing values.
     * @param pos Initial position.
     * @param text Initial text.
     * @param fontSize Initial text font size.
     * @param millis Time to live.
     * @param animate Animate text appearance.
     */
    HEText(const Vec2D& pos, const std::string& text, unsigned fontSize, unsigned millis, bool animate);

    /**
     * @brief Check text active.
     * @return True if still active.
     */
    bool isAlive() const;

    /** @brief Update internal values. */
    void update();
};

#endif // HETEXT_H


