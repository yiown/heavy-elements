#include <MoverLineal.h>

/** Lineal movement implementation. */
Vec2D MoverLineal::calculate(unsigned millisLeft) const {
    return destPos - distance * (float(millisLeft) / float(millisOut));
}

