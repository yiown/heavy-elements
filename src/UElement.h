#ifndef UELEMENT_H
#define UELEMENT_H
#include <Urho3D/Scene/Node.h>
#include <Vec2D.h>
#include <string>
#include <HEState.h>

/** @brief Underlaying element for Urho3D. */
class UElement {
private:
    /** @brief Urho3D node for the element. */
    Urho3D::Node* node_;

    /**
     * @brief Add effect to the Urho3D node.
     * @param name Effect name.
     * @param res Effect resource.
     */
    void addEffectToNode(HEState name, std::string res) const;

public:
    /** @brief Name for the element. */
    const std::string name;

    /**
     * @brief Constructor initializing underlaying structures.
     * @param name Element name.
     * @param pos Initial position.
     */
    UElement(const std::string& name, const Vec2D& pos);

    /** @brief Destructor. */
    virtual ~UElement();

    /**
     * @brief Element ID setter.
     * @param id ID for the element.
     */
    void setId(unsigned id) const;

    /**
     * @brief Element ID getter.
     * @return Element ID.
     */
    unsigned getId() const;

    /**
     * @brief Get current position.
     * @return Element position.
     */
    Vec2D getPos() const;

    /**
     * @brief Set current position.
     * @param pos Position to set.
     */
    void setPos(const Vec2D& pos) const;

    /**
     * @brief Set scale.
     * @param scale Scale to set.
     */
    void setScale(float scale) const;

    /**
     * @brief Add state visualization.
     * @param state State to add.
     */
    virtual void addState(HEState state);

    /**
     * @brief Remove state visualization.
     * @param state State to remove
     */
    virtual void removeState(HEState state);
};

#endif // UELEMENT_H


