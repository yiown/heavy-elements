#ifndef UTEXT_H
#define UTEXT_H
#include <Vec2D.h>
#include <string>
#include <Urho3D/UI/Text3D.h>

/** Urho3D text element. */
class UText {
private:
    /** @brief Urho3D scene node. */
    Urho3D::Node* node_;
    /** @brief Urho3D text node. */
    Urho3D::Text3D* text_;

public:
    /**
     * @brief Constructor initializing values.
     * @param pos Initial position.
     * @param fontSize Initial text font size.
     */
    UText(const Vec2D& pos, unsigned fontSize);

    /** @brief Destructor. */
    virtual ~UText();

    /**
     * @brief Text setter.
     * @param text Text to display.
     */
    void setText(std::string text) const;

    /**
     * @brief Color setter.
     * @param r Red component.
     * @param g Green component.
     * @param b Blue component.
     */
    void setColor(float r, float g, float b) const;
};

#endif // UTEXT_H


