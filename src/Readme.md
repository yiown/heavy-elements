Heavy Elements
==============

Code Constraints:
-----------------
- Do not use std:to_string since it does not work in Android.
- Do not use std::make_* since it does not work in LLVM.
- Stay inside C++11 since many compilers are still trying to provide C++14.
