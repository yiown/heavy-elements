#ifndef HECHLORINE_H
#define HECHLORINE_H
#include <HEElement.h>

/** @brief Chlorine element. */
class HEChlorine : public HEElement {
public:
    /**
     * @brief Constructor initializing values.
     * @param pos Initial position.
     */
    explicit HEChlorine(const Vec2D& pos) : HEElement("Cl", pos) { }
};

#endif // HECHLORINE_H


