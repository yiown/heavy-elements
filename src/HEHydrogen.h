#ifndef HEHYDROGEN_H
#define HEHYDROGEN_H
#include <HEElement.h>

/** @brief Hydrogen element. */
class HEHydrogen : public HEElement {
public:
    /**
     * @brief Constructor initializing values.
     * @param pos Initial position.
     */
    explicit HEHydrogen(const Vec2D& pos) : HEElement("H", pos) { }
};

#endif // HEHYDROGEN_H


