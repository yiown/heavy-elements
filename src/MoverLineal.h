#ifndef MOVERLINEAL_H
#define MOVERLINEAL_H
#include <Vec2D.h>
#include <Mover.h>

/** Mover with the linear algorithm. */
class MoverLineal : public Mover {
protected:
    /** Do the calculation for the mover. */
    Vec2D calculate(unsigned millisLeft) const override;
};

#endif // MOVERLINEAL_H


