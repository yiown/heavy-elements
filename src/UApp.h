#ifndef UAPP_H
#define UAPP_H
#include <Urho3D/Engine/Application.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <HEGame.h>

/** Urho3D application. */
class UApp : public Urho3D::Application {
private:
    /** @brief Resource cache facility. */
    Urho3D::ResourceCache* cache_;
    /** @brief Main scene node. */
    Urho3D::SharedPtr< Urho3D::Scene > scene_;
    /** @brief Active camera. */
    Urho3D::Camera* camera_;

    /** @brief HE game. */
    HEGame game;

    /** @brief Tracking flag. */
    bool selecting;
    /** @brief Tracking start position. */
    Urho3D::IntVector2 selStartPos;

    /**
     * @brief Process node selection.
     * @param pos Position to check.
     */
    void ProcessSelect(Urho3D::IntVector2 pos);

    /**
    * @brief Process node dragging.
    * @param pos Position to infer direction from.
    */
    void ProcessSelDrag(Urho3D::IntVector2 pos);

public:
    /**
     * @brief Constructor initializing with context.
     * @param context Context for the application.
     */
    explicit UApp(Urho3D::Context* context);

    /** @brief Setup stage. */
    void Setup() override;
    /** @brief Start game. */
    void Start() override;
    /** @brief Stop game. */
    void Stop() override;

    /**
     * @brief Create a node as a child of the scene.
     * @param name Name for the node.
     * @return Created node.
     */
    Urho3D::Node* createSceneChild(const std::string& name) const;

    /**
     * @brief Set main camera.
     * @param camera Camera set as active.
     */
    void setActiveCamera(Urho3D::Camera* camera);

    /**
     * @brief Lookup resource with cache.
     * @param name Name of the resource file.
     * @return Resource casted.
     */
    template < class T >
    T* getResource(const std::string& name) {
        return cache_->GetResource< T >(name.c_str());
    }

    /** @brief Handle update event. */
    void HandleUpdate(Urho3D::StringHash, Urho3D::VariantMap&);

    /** @brief Handle MouseDown event. */
    void HandleMouseDown(Urho3D::StringHash, Urho3D::VariantMap&);
    /** @brief Handle MouseMove event. */
    void HandleMouseMove(Urho3D::StringHash, Urho3D::VariantMap&);
    /** @brief Handle MouseUp event. */
    void HandleMouseUp(Urho3D::StringHash, Urho3D::VariantMap&);

    /** 
    * @brief Handle TouchBegin event.
    * @param data Event details.
    */
    void HandleTouchBegin(Urho3D::StringHash, Urho3D::VariantMap& data);
    /**
    * @brief Handle TouchMove event.
    * @param data Event details.
    */
    void HandleTouchMove(Urho3D::StringHash, Urho3D::VariantMap& data);
    /** @brief Handle TouchEnd event. */
    void HandleTouchEnd(Urho3D::StringHash, Urho3D::VariantMap&);
};

/** @brief Reference to the running application. */
extern UApp* app;
/** @brief ID used as KEY into the elementnode custom attributes. */
extern const std::string KEY_ID;

#endif // UAPP_H


