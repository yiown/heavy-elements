#include <USound.h>
#include <UApp.h>
#include <Urho3D/Audio/Sound.h>

/** Constructor requesting Urho3D node. */
USound::USound(std::string name, bool looped) {
    auto sound_ = app->getResource< Urho3D::Sound >("Sounds/" + name + ".ogg");
    sound_->SetLooped(looped);
    node_ = app->createSceneChild("Sound");
    soundSource_ = node_->CreateComponent< Urho3D::SoundSource >();
    soundSource_->Play(sound_);
}

/** Destructor releasing the Urho3D node. */
USound::~USound() {
    node_->Remove();
}

/** Check Urho3D sound source playing state. */
bool USound::isPlaying() {
    return soundSource_->IsPlaying();
}

