#ifndef MOVER_H
#define MOVER_H
#include <chrono>
#include <Vec2D.h>

/** Mover for animations. */
class Mover {
protected:
    /** @brief Destination position. */
    Vec2D destPos;
    /** @brief Full distance. */
    Vec2D distance;
    /** @brief Final timeout. */
    std::chrono::time_point< std::chrono::high_resolution_clock > timeout;
    /** @brief Milliseconds to animate. */
    unsigned millisOut;

    /** Do the calculation for the mover. */
    virtual Vec2D calculate(unsigned millisLeft) const = 0;

public:
    /** @brief Default constructor. */
    Mover();
    /** @brief Virtual destructor. */
    virtual ~Mover();

    /**
     * @brief Set up the mover.
     * @param pos Start position.
     * @param dest Destination.
     * @param millis Milliseconds in between.
     */
    void set(const Vec2D& pos, const Vec2D& dest, unsigned millis);

    /**
     * @brief Get current position.
     * @return Calculated position.
     */
    Vec2D getPos() const;

    /**
     * @brief Check animation finished.
     * @return True if finished.
     */
    bool done() const;
};

#endif // MOVER_H


