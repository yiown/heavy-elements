#include <UGauge.h>
#include <UApp.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Urho2D/ParticleEffect2D.h>
#include <Urho3D/UI/Font.h>

/** Construct the Urho3D node and components. */
UGauge::UGauge(const Vec2D& pos) {
    // node
    node_ = app->createSceneChild("Gauge");
    node_->SetScale(1.5f);
    node_->SetPosition(Urho3D::Vector3(pos.x, pos.y, 0));
    // shape
    auto model_ = node_->CreateComponent< Urho3D::StaticModel >();
    model_->SetModel(app->getResource< Urho3D::Model >("Models/Sphere.mdl"));
    model_->SetMaterial(app->getResource< Urho3D::Material >("Materials/Gauge.xml"));
    // effect
    emitter_ = node_->CreateChild("Radiation")->CreateComponent< Urho3D::ParticleEmitter2D >();
    emitter_->SetEffect(app->getResource< Urho3D::ParticleEffect2D >("Effects/radiation.pex"));
    // text
    auto textChild_ = node_->CreateChild("Text");
    textChild_->SetPosition(Urho3D::Vector3(0, 0, -.5));
    text_ = textChild_->CreateComponent< Urho3D::Text3D >();
    text_->SetFont(app->getResource< Urho3D::Font >("Fonts/digital-7 mono italic.ttf"), 50);
    text_->SetAlignment(Urho3D::HA_CENTER, Urho3D::VA_CENTER);
    text_->SetColor(Urho3D::Color(0, 1, 1));
}

/** Destructor releasing the Urho3D node. */
UGauge::~UGauge() {
    node_->Remove();
}

/** Set the Urho3D text node string. */
void UGauge::setValue(unsigned value, std::string text) const {
    emitter_->SetMaxParticles(value);
    text_->SetText(text.c_str());
}

