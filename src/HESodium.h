#ifndef HESODIUM_H
#define HESODIUM_H
#include <HEElement.h>

/** @brief Sodium element. */
class HESodium : public HEElement {
public:
    /**
     * @brief Constructor initializing values.
     * @param pos Initial position.
     */
    explicit HESodium(const Vec2D& pos) : HEElement("Na", pos) { }
};

#endif // HESODIUM_H


