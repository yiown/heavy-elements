#include <HEText.h>

/** Constructor passing values up. */
HEText::HEText(const Vec2D& pos, const std::string& text, unsigned fontSize, unsigned millis, bool animate) :
    UText(pos, fontSize), animate(animate), text(text) {
    ttl = std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(millis);
    animRatio = animate ? float(text.length()) / float(millis) : 0;
    if (!animate)
        setText(text);
}

/** Check time to live remaining. */
bool HEText::isAlive() const {
    return std::chrono::high_resolution_clock::now() < ttl;
}

/** Update animations. */
void HEText::update() {
    if (animate) {
        auto now = std::chrono::high_resolution_clock::now();
        if (now > ttl) {
            animate = false;
            setText(text);
        } else {
            auto millisLeft = std::chrono::duration_cast< std::chrono::milliseconds >(ttl - now).count();
            auto showText = text;
            showText.resize(showText.length() - unsigned(millisLeft * animRatio));
            setText(showText);
        }
    }
}

