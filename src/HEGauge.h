#ifndef HEGAUGE_H
#define HEGAUGE_H
#include <UGauge.h>

/** Gauge display. */
class HEGauge : public UGauge {
public:
    /**
     * @brief Constructor initializing values.
     * @param pos Gauge initial position.
     */
    explicit HEGauge(const Vec2D& pos);
};

#endif // HEGAUGE_H


