#ifndef HECHAMBER_H
#define HECHAMBER_H
#include <UChamber.h>

/** Reaction chamber. */
class HEChamber : public UChamber {
public:
    /** Reactor chamber constructor. */
    HEChamber();
};

#endif // HECHAMBER_H


