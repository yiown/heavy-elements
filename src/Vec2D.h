#ifndef VEC2D_H
#define VEC2D_H
#include <Direction.h>

/** Two dimensional cartesian vector. */
class Vec2D {
public:
    /** @brief X coordinate. */
    float x;
    /** @brief Y coordinate. */
    float y;

    /** @brief Default constructor. */
    Vec2D();

    /**
     * @brief Constructor from coordinates.
     * @param x X coordinate.
     * @param y Y coordinate.
     */
    Vec2D(float x, float y);

    /**
     * @brief Operator add.
     * @param v Vector to add.
     * @return Added vector.
     */
    Vec2D operator+(const Vec2D& v) const;

    /**
     * @brief Operator substract.
     * @param v Vector to substract.
     * @return Subtracted vector.
     */
    Vec2D operator-(const Vec2D& v) const;

    /**
     * @brief Operator divide by constant.
     * @param k Constant to divide by.
     * @return Resulting vector.
     */
    Vec2D operator/(float k) const;

    /**
     * @brief Operator multiply by constant.
     * @param k Constant to multiply by.
     * @return Resulting vector.
     */
    Vec2D operator*(float k) const;

    /**
     * @brief Operator less than.
     * @param v Vector to check against.
     * @return True if less;
     */
    bool operator<(const Vec2D& v) const;

    /**
     * @brief Operator less than or equal.
     * @param v Vector to check against.
     * @return True if less or equal;
     */
    bool operator<=(const Vec2D& v) const;

    /**
     * @brief Distance within vectors.
     * @param v Vector to calculate against.
     * @return Distance.
     */
    float distance(const Vec2D& v) const;

    /**
     * @brief Add in the direction specified.
     * @param d Direction to add.
     */
    Vec2D add(const Direction d) const;
};

#endif // VEC2D_H


