#ifndef HECARBON_H
#define HECARBON_H
#include <HEElement.h>

/** @brief Carbon element. */
class HECarbon : public HEElement {
public:
    /**
     * @brief Constructor initializing values.
     * @param pos Initial position.
     */
    explicit HECarbon(const Vec2D& pos) : HEElement("C", pos) { }
};

#endif // HECARBON_H


