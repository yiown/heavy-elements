#ifndef HESTATE_H
#define HESTATE_H
#include <string>

/** Element states. */
enum class HEState : char {
    SELECTED = 'S',
    MOVING = 'M',
    REACT = 'R',
    DROPING = 'D',
    DROPPED = 'X'
};

/**
 * @brief Convert the state enum value into a full string.
 * @param state State to convert.
 * @return Full string from value.
 */
std::string toString(HEState state);

#endif // HESTATE_H


