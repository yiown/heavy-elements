#ifndef DIRECTION_H
#define DIRECTION_H

/** Enum for directions. */
enum Direction {
    LEFT,
    RIGHT,
    UP,
    DOWN
};

#endif // DIRECTION_H


