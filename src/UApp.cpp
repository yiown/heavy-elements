#include <UApp.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Input/InputEvents.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/Resource/XMLFile.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/UI/Text3D.h>

/** Macro to expand main entry point. */
URHO3D_DEFINE_APPLICATION_MAIN(UApp)

/** Global reference to the application. */
UApp* app = nullptr;

/** ID KEY for the element node custom attributes. */
const std::string KEY_ID = "HEID";
const int SENSITIVITY = 10;

/** Check selection event on position by ray tracing from the camera to the position specified. */
void UApp::ProcessSelect(Urho3D::IntVector2 pos) {
    // check there is no UI element in front of the cursor
    auto ui = GetSubsystem< Urho3D::UI >();
    if (!ui->GetElementAt(pos, true)) {
        auto graphics = GetSubsystem< Urho3D::Graphics >();
        auto cameraRay = camera_->GetScreenRay(
            float(pos.x_) / graphics->GetWidth(), float(pos.y_) / graphics->GetHeight());
        // look only for geometry objects and only get the first hit
        Urho3D::PODVector< Urho3D::RayQueryResult > results;
        Urho3D::RayOctreeQuery query(results, cameraRay, Urho3D::RAY_TRIANGLE, 100, Urho3D::DRAWABLE_GEOMETRY);
        scene_->GetComponent< Urho3D::Octree >()->RaycastSingle(query);
        if (results.Size()) {
            auto v = results[0].drawable_->GetNode()->GetVar(KEY_ID.c_str());
            if (!v.IsEmpty()) {
                // send event to the game
                game.select(v.GetUInt());
                // activate tracking  
                selecting = true;
                selStartPos = pos;
            }
        }
    }
}

/** Send a drag event from the tracking start position in the direction of the current position. */
void UApp::ProcessSelDrag(Urho3D::IntVector2 pos) {
    if (selecting) {
        auto mov = selStartPos - pos;
        if (abs(mov.x_) > SENSITIVITY || abs(mov.y_) > SENSITIVITY) {
            // stop tracking  
            selecting = false;
            // send event to the game
            if (abs(mov.x_) > abs(mov.y_)) {
                if (mov.x_ < 0)
                    game.selDrag(Direction::RIGHT);
                else
                    game.selDrag(Direction::LEFT);
            } else {
                if (mov.y_ > 0)
                    game.selDrag(Direction::UP);
                else
                    game.selDrag(Direction::DOWN);
            }
        }
    }
}

/** Constructor initializing values and the global reference. */
UApp::UApp(Urho3D::Context* context) : Urho3D::Application(context), selecting(false) {
    app = this;
}

/** Setup app to start maximized but not fullscreen. */
void UApp::Setup() {
    engineParameters_["FullScreen"] = false;
    engineParameters_["WindowResizable"] = true;
    // default for emscripten so making default for other platforms
    engineParameters_["WindowWidth"] = 1024;
    engineParameters_["WindowHeight"] = 768;
}

/** Support objects created, start application specific content. */
void UApp::Start() {
    // the mouse must be in cursor mode before setting the UI.
    auto input_ = GetSubsystem< Urho3D::Input >();
    input_->SetMouseVisible(true);

    // resources origin.  
    cache_ = GetSubsystem< Urho3D::ResourceCache >();

    // use the default style from Urho3D.
    GetSubsystem< Urho3D::UI >()->GetRoot()->SetDefaultStyle(
                                    cache_->GetResource< Urho3D::XMLFile >("UI/DefaultStyle.xml"));

    // grab mouse
    input_->SetMouseGrabbed(true);

    // setup scene to render. 
    scene_ = new Urho3D::Scene(context_);
    scene_->CreateComponent< Urho3D::Octree >();

    // Create the light
    {
        auto lightNode = scene_->CreateChild("Light");
        lightNode->SetPosition(Urho3D::Vector3(6, 6, -20));
        auto light = lightNode->CreateComponent< Urho3D::Light >();
        light->SetLightType(Urho3D::LIGHT_POINT);
        light->SetRange(1000);
    }

    // start the game
    game.start();

    // subscribe to events
    SubscribeToEvent(Urho3D::E_UPDATE,URHO3D_HANDLER(UApp,HandleUpdate));
    SubscribeToEvent(Urho3D::E_MOUSEBUTTONDOWN,URHO3D_HANDLER(UApp,HandleMouseDown));
    SubscribeToEvent(Urho3D::E_MOUSEMOVE,URHO3D_HANDLER(UApp,HandleMouseMove));
    SubscribeToEvent(Urho3D::E_MOUSEBUTTONUP,URHO3D_HANDLER(UApp,HandleMouseUp));
    SubscribeToEvent(Urho3D::E_TOUCHBEGIN, URHO3D_HANDLER(UApp, HandleTouchBegin));
    SubscribeToEvent(Urho3D::E_TOUCHMOVE, URHO3D_HANDLER(UApp, HandleTouchMove));
    SubscribeToEvent(Urho3D::E_TOUCHEND,URHO3D_HANDLER(UApp,HandleTouchEnd));
}

/** Application clean up measures. */
void UApp::Stop() {}

/** Create a scene child node. */
Urho3D::Node* UApp::createSceneChild(const std::string& name) const {
    return scene_->CreateChild(name.c_str());
}

/** Create a rendering viewport for the camera and register it as active. */
void UApp::setActiveCamera(Urho3D::Camera* camera) {
    camera_ = camera;
    auto renderer = app->GetSubsystem< Urho3D::Renderer >();
    Urho3D::SharedPtr< Urho3D::Viewport > viewport(new Urho3D::Viewport(context_, scene_, camera_));
    renderer->SetViewport(0, viewport);
}

/** Update application and game. */
void UApp::HandleUpdate(Urho3D::StringHash, Urho3D::VariantMap&) {
    game.update();
}

/** Send select event and activate mouse tracking. */
void UApp::HandleMouseDown(Urho3D::StringHash, Urho3D::VariantMap&) {
    ProcessSelect(GetSubsystem< Urho3D::UI >()->GetCursorPosition());
}

/** Check mouse tracking movement. */
void UApp::HandleMouseMove(Urho3D::StringHash, Urho3D::VariantMap&) {
    ProcessSelDrag(GetSubsystem< Urho3D::UI >()->GetCursorPosition());
}

/** End mouse tracking. */
void UApp::HandleMouseUp(Urho3D::StringHash, Urho3D::VariantMap&) {
    selecting = false;
}

/** Start selecting event and save the start touch position. */
void UApp::HandleTouchBegin(Urho3D::StringHash, Urho3D::VariantMap& data) {
    ProcessSelect(Urho3D::IntVector2(data[Urho3D::TouchBegin::P_X].GetInt(), data[Urho3D::TouchBegin::P_Y].GetInt()));
}

/** Check selection based on the touch position while moving. */
void UApp::HandleTouchMove(Urho3D::StringHash, Urho3D::VariantMap& data) {
    ProcessSelDrag(Urho3D::IntVector2(data[Urho3D::TouchMove::P_X].GetInt(), data[Urho3D::TouchMove::P_Y].GetInt()));
}

/** End a selection event. */
void UApp::HandleTouchEnd(Urho3D::StringHash, Urho3D::VariantMap&) {
    selecting = false;
}

