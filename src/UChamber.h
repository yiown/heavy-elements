#ifndef UCHAMBER_H
#define UCHAMBER_H
#include <Urho3D/Scene/Node.h>

/** Chamber Urho3D implementation. */
class UChamber {
private:
    /** @brief Chamber node. */
    Urho3D::Node* chamberNode_;
    /** @brief Ambient node. */
    Urho3D::Node* zoneNode_;
public:
    /** @brief Chamber constructor. */
    UChamber();
};

#endif // UCHAMBER_H


