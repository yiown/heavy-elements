#include <HEBoard.h>
#include <HEHydrogen.h>
#include <HENitrogen.h>
#include <HECarbon.h>
#include <HESodium.h>
#include <HEChlorine.h>
#include <HEState.h>
#include <HEText.h>
#include <algorithm>
#include <Utils.h>

const unsigned SLIDE_MILLIS = 300;
const unsigned DROP_MILLIS = 300;

/** Convert from HEID to board position. */
Vec2D HEBoard::idToPos(unsigned id) const {
    return Vec2D(float(id % unsigned(size.x)), float(id / unsigned(size.x)));
}

/** Lookup element. */
std::shared_ptr< HEElement >& HEBoard::getElement(const Vec2D& pos) {
    return elements[unsigned(pos.y)][unsigned(pos.x)];
}

/** Slide element to board position. */
void HEBoard::slideTo(std::shared_ptr< HEElement > element, Vec2D pos) {
    element->slideTo(pos * 1.5, SLIDE_MILLIS);
    element->setId(unsigned(pos.y * size.x + pos.x));
    getElement(pos) = element;
}

/** Generate random element. */
void HEBoard::rndElement(Vec2D pos) {
    auto initPos = pos + Vec2D(0, size.y * 2.0f);
    HEElement* element;
    switch (rnd(gen)) {
    case 1:
        element = new HENitrogen(initPos);
        break;
    case 2:
        element = new HECarbon(initPos);
        break;
    case 3:
        element = new HESodium(initPos);
        break;
    case 4:
        element = new HEChlorine(initPos);
        break;
    default:
        element = new HEHydrogen(initPos);
        break;
    }
    slideTo(std::shared_ptr< HEElement >(element), pos);
}

/** Chain react the elements. */
void HEBoard::react(std::vector< std::shared_ptr< HEElement > >& reactions) {
    if (reactions.size() >= 3) {
        std::string soundName;
        unsigned eScore = 0;
        switch (reactions.size()) {
        case 3:
            soundName = "three";
            eScore = 25;
            break;
        case 4:
            soundName = "four";
            eScore = 50;
            break;
        case 5:
            soundName = "four";
            eScore = 75;
            break;
        case 6:
            soundName = "four";
            eScore = 150;
            break;
        case 7:
            soundName = "four";
            eScore = 300;
            break;
        }
        for (auto& e : reactions) {
            e->addState(HEState::REACT);
            surges.emplace_back(new HEText(e->getPos(), toString(eScore, 0), 60, DROP_MILLIS * 2, false));
            score += eScore;
        }
        sounds.emplace_back(new HESound(soundName, false));
    }
}

/** Two way reaction. */
void HEBoard::react(std::vector< std::shared_ptr< HEElement > >& reactions, Vec2D pos, Direction a, Direction b) {
    // react in both directions
    react(reactions, pos, a);
    reactions.push_back(getElement(pos));
    react(reactions, pos, b);
}

/** Recursive directional reactions. */
void HEBoard::react(std::vector< std::shared_ptr< HEElement > >& reactions, Vec2D pos, Direction direction) {
    auto npos = pos.add(direction);
    if (Vec2D(0, 0) <= npos && npos < size) {
        auto nextElement = getElement(npos);
        if (nextElement) {
            auto element = getElement(pos);
            if (nextElement->isAlive() && element->isAlive()
                && (element->reactsTo(nextElement) || nextElement->reactsTo(element))) {
                reactions.push_back(nextElement);
                react(reactions, npos, direction);
            }
        }
    }
}

/** Check selections and trigger the switcheroo. */
void HEBoard::checkSelection() {
    // check pair
    if (selected.size() == 2) {
        auto a = selected[0].lock();
        auto b = selected[1].lock();
        if (a->isAlive() && b->isAlive()
            && !a->hasState(HEState::MOVING) && !b->hasState(HEState::MOVING)
            && idToPos(a->getId()).distance(idToPos(b->getId())) == 1) {
            // do the switcheroo
            auto apos = idToPos(a->getId());
            slideTo(a, idToPos(b->getId()));
            slideTo(b, apos);
        } else {
            // invalid selections, cancel the first one
            a->removeState(HEState::SELECTED);
            selected.erase(selected.begin());
        }
    }
}

/** Constructor initializing internal values. */
HEBoard::HEBoard(unsigned width, unsigned height) :
    gen(rd()), rnd(1, 5), size(float(width), float(height)), playing(false), score(0) {}

/** Start the board. */
void HEBoard::start() {
    // set state
    playing = true;
    // initialize HUD
    score = 0;
    energy = std::unique_ptr< HEText >(new HEText(Vec2D(-4, 10), "", 150, 0, false));
    energy->setColor(0, 1, 1);
    darkMatter = std::unique_ptr< HEGauge >(new HEGauge(Vec2D(-5, 5)));
    timeout = std::chrono::high_resolution_clock::now() + std::chrono::seconds(60);
    // randomize board
    {
        Vec2D pos;
        elements.resize(unsigned(size.y));
        for (pos.y = 0; pos.y < size.y; pos.y++) {
            elements[unsigned(pos.y)].resize(unsigned(size.x));
            for (pos.x = 0; pos.x < size.x; pos.x++)
                rndElement(pos);
        }
    }
    // clear any reactions
    bool hasReactions;
    do {
        hasReactions = false;
        std::vector< std::shared_ptr< HEElement > > reactions;
        Vec2D pos;
        for (pos.y = 0; pos.y < size.y; pos.y++)
            for (pos.x = 0; pos.x < size.x; pos.x++) {
                // react horizontaly
                react(reactions, pos, Direction::LEFT, Direction::RIGHT);
                if (reactions.size() >= 3) {
                    rndElement(pos);
                    hasReactions = true;
                }
                reactions.clear();
                // react vertically  
                react(reactions, pos, Direction::DOWN, Direction::UP);
                if (reactions.size() >= 3) {
                    rndElement(pos);
                    hasReactions = true;
                }
                reactions.clear();
            }
    } while (hasReactions);
}

/** Update the board elements and states. */
void HEBoard::update() {
    // update elements
    auto moving = false;
    for (auto& j : elements)
        for (auto& e : j) {
            e->update();
            moving = moving || e->hasState(HEState::MOVING);
        }
    // check sounds
    sounds.erase(std::remove_if(sounds.begin(), sounds.end(), [](std::shared_ptr< HESound >& s) {
                                    return !s->isPlaying();
                                }), sounds.end());
    // check surges 
    surges.erase(std::remove_if(surges.begin(), surges.end(), [](std::unique_ptr< HEText >& t) {
                                    return !t->isAlive();
                                }), surges.end());
    // check state
    if (!playing)
        return;
    // energy and dark matter
    energy->setText("<Output>\n" + toString(score, 0) + " KW");
    auto remaining = timeout - std::chrono::high_resolution_clock::now();
    auto millisLeft = std::chrono::duration_cast< std::chrono::milliseconds >(remaining).count();
    darkMatter->setValue(unsigned(millisLeft / 100.0f), toString(millisLeft > 0 ? millisLeft / -100000.0f : .0f, 6));

    // check end game
    if (millisLeft <= 0) {
        sounds.clear();
        playing = false;
    }

    // check reactions
    if (!moving) {
        std::vector< std::shared_ptr< HEElement > > reactions;
        Vec2D pos;
        for (pos.y = 0; pos.y < size.y; pos.y++)
            for (pos.x = 0; pos.x < size.x; pos.x++) {
                // react horizontaly
                react(reactions, pos, Direction::LEFT, Direction::RIGHT);
                react(reactions);
                reactions.clear();
                // react vertically  
                react(reactions, pos, Direction::DOWN, Direction::UP);
                react(reactions);
                reactions.clear();
            }
    }

    // advance element states
    for (auto& j : elements)
        for (auto& e : j) {
            // reacted to droping  
            if (e->hasState(HEState::REACT) && e->isAlive())
                e->drop(DROP_MILLIS);
            // dropped to gone
            if (e->hasState(HEState::DROPPED)) {
                auto pos = idToPos(e->getId());
                auto slideFrom = pos + Vec2D(0, 1);
                if (slideFrom.y >= size.y)
                // randomize element  
                    rndElement(pos);
                else {
                    // pull from above
                    auto element = getElement(pos);
                    slideTo(getElement(slideFrom), pos);
                    slideTo(element, slideFrom);
                }
            }
        }

    // check selections
    selected.erase(std::remove_if(selected.begin(), selected.end(), [](std::weak_ptr< HEElement >& e) {
                                      return e.expired();
                                  }), selected.end());
    if (!moving && selected.size() == 2) {
        auto a = selected[0].lock();
        auto b = selected[1].lock();
        if (!a->hasState(HEState::MOVING) && !b->hasState(HEState::MOVING)) {
            if (a->isAlive() && b->isAlive()) {
                // still alive, switcheroo back
                auto apos = idToPos(a->getId());
                slideTo(a, idToPos(b->getId()));
                slideTo(b, apos);
            }
            a->removeState(HEState::SELECTED);
            b->removeState(HEState::SELECTED);
            selected.clear();
        }
    }
}

/** Select board item by ID. */
void HEBoard::select(unsigned heId) {
    // check state
    if (!playing)
        return;
    // board element selection 
    if (heId < size.x * size.y && selected.size() < 2) {
        auto pos = idToPos(heId);
        auto& element = getElement(pos);
        // check selected
        if (selected.size() == 0 || selected[0].expired() || selected[0].lock()->getId() != element->getId()) {
            element->addState(HEState::SELECTED);
            selected.push_back(element);
            checkSelection();
        }
    }
}

/** Drag selected element in the direction specified. */
void HEBoard::selDrag(Direction d) {
    // check state
    if (!playing)
        return;
    // fetch pointed element  
    if (selected.size() == 1 && !selected[0].expired()) {
        auto a = selected[0].lock();
        auto npos = idToPos(a->getId()).add(d);
        if (Vec2D(0, 0) <= npos && npos < size) {
            auto nextElement = getElement(npos);
            if (nextElement) {
                nextElement->addState(HEState::SELECTED);
                selected.push_back(nextElement);
                checkSelection();
            }
        }
    }
}

/** Playing status getter. */
bool HEBoard::isPlaying() const {
    return playing;
}

/** Score getter. */
unsigned HEBoard::getScore() const {
    return score;
}

