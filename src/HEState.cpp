#include <HEState.h>

/** State char to string object. */
std::string toString(HEState state) {
    return std::string(1, char(state));
}

