#ifndef MOVERSLIDEIN_H
#define MOVERSLIDEIN_H
#include <Vec2D.h>
#include <Mover.h>

/** Mover with the smooth slide in algorithm. */
class MoverSlideIn : public Mover {
protected:
    /** Do the calculation for the mover. */
    Vec2D calculate(unsigned millisLeft) const override;
};

#endif // MOVERSLIDEIN_H


