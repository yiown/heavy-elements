#ifndef HEBOARD_H
#define HEBOARD_H
#include <vector>
#include <memory>
#include <random>
#include <Direction.h>
#include <HEElement.h>
#include <HESound.h>
#include <HEGauge.h>
#include <HEText.h>

/** @brief Reaction chamber board. */
class HEBoard {
private:
    /** @brief Random non deterministic device. */
    std::random_device rd;
    /** @brief Mersenne Twister pseudo-random generator. */
    std::mt19937 gen;
    /** @brief Uniform distribution generator. */
    std::uniform_int_distribution< int > rnd;

    /** @brief Board sounds. */
    std::vector< std::shared_ptr< HESound > > sounds;

    /** @brief Board size in elements. */
    Vec2D size;
    /** @brief Board elements. */
    std::vector< std::vector< std::shared_ptr< HEElement > > > elements;

    /** @brief Board timeout. */
    std::chrono::time_point< std::chrono::high_resolution_clock > timeout;
    /** @brief Board playing state. */
    bool playing;

    /** @brief Selected elements to action on. */
    std::vector< std::weak_ptr< HEElement > > selected;

    /** @brief Countdown timer. */
    std::unique_ptr< HEGauge > darkMatter;

    /** @brief Score keeper. */
    unsigned score;
    /** @brief Score counter. */
    std::unique_ptr< HEText > energy;
    /** @brief Scores over the board. */
    std::vector< std::unique_ptr< HEText > > surges;

    /**
     * @brief HEID to board position converted.
     * @param id HEID.
     * @return Board coordinates.
     */
    Vec2D idToPos(unsigned id) const;

    /**
    * @brief Element lookup by vector.
    * @param pos Position of the element.
    * @return Element found.
    */
    std::shared_ptr< HEElement >& getElement(const Vec2D& pos);

    /**
     * @brief Slide element into board position.
     * @param element Element to slide.
     * @param pos Board position.
     */
    void slideTo(std::shared_ptr< HEElement > element, Vec2D pos);

    /**
     * @brief Randomize an element.
     * @param pos Board position.
     */
    void rndElement(Vec2D pos);

    /**
    * @brief Chain react the elements.
    * @param reactions Reactions found.
    */
    void react(std::vector< std::shared_ptr< HEElement > >& reactions);

    /**
     * @brief Two way reactions.
     * @param reactions Reactions found.
     * @param pos Initial position.
     * @param a Reaction way one.
     * @param b Reaction way two.
     */
    void react(std::vector< std::shared_ptr< HEElement > >& reactions, Vec2D pos, Direction a, Direction b);

    /**
     * @brief Single direction reaction.
     * @param reactions Reactions found.
     * @param pos Initial position.
     * @param direction Reaction direction.
     */
    void react(std::vector< std::shared_ptr< HEElement > >& reactions, Vec2D pos, Direction direction);

    /** @brief Check selected elements and trigger the movement. */
    void checkSelection();

public:
    /**
     * @brief Constructor initializing values.
     * @param width Board width.
     * @param height Board height.
     */
    HEBoard(unsigned width, unsigned height);

    /** @brief Start the board play. */
    void start();
    /** @brief Update the board elements. */
    void update();

    /**
     * @brief Select by ID.
     * @param heId ID of the selection.
     */
    void select(unsigned heId);

    /**
    * @brief Drag selection.
    * @param d Dragging direction.
    */
    void selDrag(Direction d);

    /**
     * @brief Playing state gatter.
     * @return True if the board is running.
     */
    bool isPlaying() const;

    /**
     * @brief Score getter.
     * @return Energy score.
     */
    unsigned getScore() const;
};

#endif // HEBOARD_H


