#ifndef HESOUND_H
#define HESOUND_H
#include <USound.h>

/** Sound abstraction. */
class HESound : public USound {
public:
    /**
     * @brief Constructor initializing values.
     * @param name Sound name.
     * @param looped Play looped.
     */
    HESound(std::string name, bool looped);
};

#endif // HESOUND_H


